﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHpScript : MonoBehaviour {

	public float TotalHp;
	public float EnemyHp;
	public float Damage;
	public Text tex;
	// Use this for initialization
	void Start () {
		EnemyHp = TotalHp;
	}

	// Update is called once per frame
	void Update(){
		tex.text = "HP : "+ EnemyHp.ToString()+"/"+TotalHp.ToString();
		if (Input.GetButtonDown ("Jump")) {
			Damage = 5;
			TakeDamage (Damage);
		}
	}
	public  void Attack(){
		//TakeDamage ();
	}
	public void TakeDamage(float damage)
	{
		EnemyHp -= damage;
		transform.localScale = new Vector3((EnemyHp / TotalHp), 1, 1);

	}
	public void Dua(){
		Damage = 2;
	}

}

