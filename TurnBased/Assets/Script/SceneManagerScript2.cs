﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneManagerScript2 : MonoBehaviour {

	public Text ValueText;
	public int index;
	public Image black;
	public Animator anim;
	public GameObject screen;
	public bool change;

	private void Start()
	{
		ValueText.text = PersistentManagerScript.Instance.Value.ToString ();
		StartCoroutine("Faded");
	}
	void Update(){
		
	}

	public void GoToFirstScene()
	{
		Debug.Log ("Bisa Bisa");
		SceneManager.LoadScene ("Singleton1st");
		PersistentManagerScript.Instance.Value++;
		StartCoroutine(Fading());


		}

	public void GoToSecondScene()
	{
		
		SceneManager.LoadScene ("Singleton2nd");
		PersistentManagerScript.Instance.Value++;
		StartCoroutine("Faded");
	}

	IEnumerator Fading()
	{
		
			anim.SetBool ("Fade", true);
			yield return new WaitUntil (() => black.color.a == 1);
			SceneManager.LoadScene (index);
			screen.SetActive (false);

	}
	IEnumerator Faded()
	{
		//yield return new WaitForSeconds (2f);
		yield return new WaitUntil (() => black.color.a == 0);
		screen.SetActive (false);
	}
}
