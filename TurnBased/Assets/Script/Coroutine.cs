﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coroutine : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine ("coRoutineTest");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator coRoutineTest (){
		Debug.Log ("Coroutine Mulai");
		yield return new WaitForSeconds (3f);
		Debug.Log ("Coroutine Berhenti");
	}
}
