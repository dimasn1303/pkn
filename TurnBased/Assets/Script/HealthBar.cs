﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	public float TotalHp;
	public float CurrentHp;
	public float Damage;
	public Text tex;
	// Use this for initialization
	void Start () {
		CurrentHp = TotalHp;
	}

	// Update is called once per frame
	void Update(){
		//tex.text = CurrentHp.ToString ();
		if (Input.GetMouseButtonDown (0)) {
			Attack ();
		}
	}
	public  void Attack(){
		TakeDamage ();
	}
	public void TakeDamage()
	{
		CurrentHp -= 2;
		transform.localScale = new Vector3((CurrentHp / TotalHp), 1, 1);

	}
	public void Dua(){
		Damage = 2;
	}
}
