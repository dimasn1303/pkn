﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript: MonoBehaviour {
	
	public static GameManagerScript gameManager{ get; private set;}

	public int[] slot;
	public int[] paralyzedTurn;
	public int[] burnTurn;
	public int turn;
	public int PlayerHealth;
	public int EnemyHealth;

	void Awake(){
		if (gameManager == null) {
			DontDestroyOnLoad (gameObject);
			gameManager = this;
		} else if (gameManager != this) {
			Destroy (this);
		}
	}
}
