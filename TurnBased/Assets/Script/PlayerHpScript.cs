﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHpScript: MonoBehaviour {

	public float TotalHp;
	public float PlayerHp;
	public float Damage;
	public Text tex;
	// Use this for initialization
	void Start () {
		PlayerHp = TotalHp;
	}

	// Update is called once per frame
	void Update(){
		tex.text = "HP : "+ PlayerHp.ToString()+"/"+TotalHp.ToString();
		if (Input.GetMouseButtonDown (0)) {
			Damage = 5;
			TakeDamage (Damage);
		}
	}
	public  void Attack(){
		//TakeDamage ();
	}
	public void TakeDamage(float damage)
	{
		PlayerHp -= damage;
		transform.localScale = new Vector3((PlayerHp / TotalHp), 1, 1);

	}
	public void Dua(){
		Damage = 2;
	}
}

