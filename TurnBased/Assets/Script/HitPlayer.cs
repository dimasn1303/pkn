﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPlayer : MonoBehaviour {

	public GameObject playerObject;
	public GameObject hitMarkerPrefab;
//	public Color[] markerColors;
	public float markerKillTimer;

	public void Update()
	{
		if(Input.GetMouseButtonDown(0))
			{
			HitNow ();
		}
	}
	public void HitNow()
	{
		GameObject newMarker = Instantiate (hitMarkerPrefab, playerObject.transform.position, Quaternion.identity);
		newMarker.SetActive (true);
		newMarker.GetComponent<DamageMarkerController> ().SetTextAndMove (Random.Range (0, 101).ToString ());
		Destroy (newMarker.gameObject, markerKillTimer);
	}
}
