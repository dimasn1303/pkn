﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUp : MonoBehaviour {

	public bool On;

	void Start(){
		PlayerHpSliderScript player = gameObject.GetComponent<PlayerHpSliderScript> ();
		On = player.isDead;
	}

	void Update () {
		if (On) {
			gameObject.SetActive (true);
		}
	}
}
