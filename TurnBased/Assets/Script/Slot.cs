﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler {
	//public SlotController sControl;
	public int SlotID;
	public int elemen;

	public GameObject item {
		get {
			if(transform.childCount>0){
				return transform.GetChild (0).gameObject;
			}
			return null;
		}
	}

	#region IDropHandler implementation
	public void OnDrop (PointerEventData eventData)
	{
		
		if(!item){
			DragHandeler.itemBeingDragged.transform.SetParent (transform);
			ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject,null,(x,y) => x.HasChanged ());

			Elemental sadas = item.GetComponent<Elemental> ();
			elemen = sadas.ID;
		}

		if (item) {
			Elemental sadas = item.GetComponent<Elemental> ();
			elemen = sadas.ID;
		}
			
	}
	#endregion
}
