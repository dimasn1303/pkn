﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageMarkerController : MonoBehaviour {

	private Text myText;
	[SerializeField]private float moveAmt;
	[SerializeField]private float movespeed;
	private Vector3[] moveDirs;
	private Vector3 mymoveDir;
	private bool canMove = false; 
	// Use this for initialization
	public void Start () 
	{
		moveDirs = new Vector3[] {
			transform.up,
			(transform.up + transform.right),
			(transform.up + -transform.right)
		};
		mymoveDir = moveDirs [Random.Range (0, moveDirs.Length)];
	}
	
	// Update is called once per frame
	public void Update () {
		if (canMove)transform.position = Vector3.MoveTowards (transform.position, transform.position + mymoveDir, moveAmt * (movespeed * Time.deltaTime));
	}
	public void SetTextAndMove(string textStr)
	{
		myText = GetComponentInChildren<Text> ();
//		myText.color = textColour;
		myText.text = textStr;
		canMove = true;
	}
}
