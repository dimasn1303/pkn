﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHpSliderScript : MonoBehaviour {

	public Slider healthBarSlider;
	public int currentHealth;
	public int maxHealth;
	public Text healthText;
	public Text messageField;
	public GameObject window;
	public Text[] tex;
	// Use this for initialization
	void Start () {
		if (GameManagerScript.gameManager.turn == 1) {
			currentHealth = maxHealth;
		} else {
			currentHealth = GameManagerScript.gameManager.EnemyHealth;
		}
	}

	// Update is called once per frame

	void Update () {
		healthBarSlider.maxValue = maxHealth;
		healthBarSlider.value = currentHealth;
		healthText.text = "HP: "+ currentHealth.ToString () + "/" + maxHealth.ToString ();
		GameManagerScript.gameManager.EnemyHealth = currentHealth;
		if (currentHealth <= 0) 
		{
			Show ("Player Win");
			Time.timeScale = 0;
		}
//		if (Input.GetButtonDown ("Jump")) {
//
//			currentHealth -= 2;
//		}	
	}
	public void Damage(){
		currentHealth -= 2;
	}
	public void Heal(){
		currentHealth += 4;
	}
	public void Show (string message)
	{
		messageField.text = message;
		window.SetActive (true);
	}
	public void Hide()
	{
		window.SetActive (false);
	}
	public void Burned(int slot){
		if(GameManagerScript.gameManager.turn>1){
			if(GameManagerScript.gameManager.burnTurn[slot] >= GameManagerScript.gameManager.turn){
				currentHealth -= 1;
				tex[slot-3].text = "Burning";
				Debug.Log ("Enemy Burned");
			}
		}
	}
}
