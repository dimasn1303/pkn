﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Sprites;

public class BotElemental : MonoBehaviour {

	enum elements{Fire = 1, Lightning = 2, Water = 3, Earth = 4}
	public int botChooseA;
	public int botChooseB;
	public int botChooseC;
	public Sprite[] ElementImage;

	public GameObject[] botChooseimage;
	// Use this for initialization

	public void BotChoose(){
		botChooseA = Random.Range(0,4);
		Debug.Log (botChooseA);
			botChooseimage[0].GetComponent<Image> ().sprite = ElementImage [botChooseA];
			
		do{
			botChooseB = Random.Range (0,4);
			Debug.Log ("botB = "+botChooseB);
		}while(botChooseB == botChooseA);
			botChooseimage[1].GetComponent<Image> ().sprite = ElementImage [botChooseB];
			
		do{
			botChooseC = Random.Range(0,4);
			Debug.Log ("botC = "+botChooseC);
		}while(botChooseC == botChooseA || botChooseC == botChooseB);
			botChooseimage[2].GetComponent<Image> ().sprite = ElementImage [botChooseC];			
	}

	void Start(){
		Debug.Log ("Mulai");
		BotChoose ();
	}
}
