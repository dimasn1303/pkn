﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScissorRockPaper : MonoBehaviour {
	enum elements {Fire = 1, Lightning= 2, Water=3}
	private int playerChoose = -1;
	private int botChoose = -1;

	private bool playersTurn = true;
	public GameObject WinnerText;
	public Sprite fireimage, lightningimage, waterimage;
	public GameObject botChooseImage;
	// Update is called once per frame
	void CheckWinner()
	{
		if (playerChoose == botChoose) {
			//draw
			WinnerText.GetComponent<Text>().text = "draw";
		} else if (playerChoose == (int)elements.Water && botChoose == (int)elements.Fire) 
		{
			//Playerwin
			WinnerText.GetComponent<Text>().text = "Playerwin";
		} else if (playerChoose == (int)elements.Water && botChoose == (int)elements.Lightning) 
		{
			//Botwin
			WinnerText.GetComponent<Text>().text = "Botwin";
		} else if (playerChoose == (int)elements.Lightning && botChoose == (int)elements.Fire) 
		{
			//Botwin
			WinnerText.GetComponent<Text>().text = "Botwin";
		}else if (playerChoose == (int)elements.Lightning && botChoose == (int)elements.Water) 
		{
			//Botwin
			WinnerText.GetComponent<Text>().text = "Playerwin";
		}else if (playerChoose == (int)elements.Fire && botChoose == (int)elements.Water)
		{
			//playerwin
			WinnerText.GetComponent<Text>().text = "Playerwin";
		}else if (playerChoose == (int)elements.Fire && botChoose == (int)elements.Lightning)
		{
			//playerwin
			WinnerText.GetComponent<Text>().text = "Botwin";
		}
	}
	public void BotChoose()
	{
		botChoose = Random.Range (1, 3);

		if (botChoose == 1) 
		{
			botChooseImage.GetComponent<Image> ().sprite = fireimage;
		} else if (botChoose == 2) 
		{
			botChooseImage.GetComponent<Image> ().sprite = lightningimage;
		} else 
		{
			botChooseImage.GetComponent<Image>().sprite = waterimage;
		}
	}

	void Update () 
	{
		if (playersTurn && playerChoose == -1)
			return;
		else 
		{
			BotChoose ();
			CheckWinner ();
			playerChoose = -1;
			playersTurn = true;
		}
	}


	public void PlayerChoose(int choose) 
	{
		playerChoose = choose;
		playersTurn = false;
	}

}
