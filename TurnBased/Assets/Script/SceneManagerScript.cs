﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneManagerScript : MonoBehaviour {
//	public int index;
	public Image black;
	public Animator anim;
	public GameObject screen;

//	void Awake(){
//		if (gameManager == null) {
//			DontDestroyOnLoad (gameObject);
//			gameManager = this;
//		} else if (gameManager != this) {
//			Destroy (this);
//		}
//	}
	void Start()
	{
		StartCoroutine("Faded");	
	}
	void Update(){
		//Debug.Log (SceneManager.GetActiveScene ().buildIndex);

	}

	public void NextLevel(){
		
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
		StartCoroutine("Faded");
	}

	public void PrevLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex - 1);
		StartCoroutine("Faded");	
	}

	public void BattleLevel(){
		SceneManager.LoadScene("BattleScene");
		GameManagerScript.gameManager.turn += 1;
		StartCoroutine("Fading");
	}

	public void PlayAgain(){
		StartCoroutine("Fading");
		SceneManager.LoadScene (0);
		GameManagerScript.gameManager.turn = 0;
		for(int i = 0;i <=5;i++){
			GameManagerScript.gameManager.burnTurn [i] = 0;
			GameManagerScript.gameManager.paralyzedTurn [i] = 0;
		}

	}


	public void ExitGame(){
		Debug.Log ("Exit From Game");
		Application.Quit ();
	}
	IEnumerator Fading()
	{
		screen.SetActive (true);
		anim.SetBool ("Fade", true);
		yield return new WaitUntil (() => black.color.a == 1);
//		SceneManager.LoadScene (index);
		screen.SetActive (false);

	}
	IEnumerator Faded()
	{
		Time.timeScale = 1;
		screen.SetActive (true);
		yield return new WaitUntil (() => black.color.a == 0);
		screen.SetActive (false);
	}
}
