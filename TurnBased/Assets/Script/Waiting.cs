﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Waiting : MonoBehaviour {

	public Image image;

	void Update(){
		if(Input.GetKeyDown(KeyCode.Space)){
			StopCoroutine (Fadding (5f));
			StartCoroutine (Fadding (5f));
		}
	}

	IEnumerator Fadding(float time){
		float i = 0;
		while (i <= 1) {
			image.color = new Color (0.0f, 0.0f, 0.0f, i);
			i += 0.2f;
			yield return new WaitForSeconds (time/5);
		}
	}
}
