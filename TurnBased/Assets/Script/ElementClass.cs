﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Sprites;

public class ElementClass : MonoBehaviour {

	enum elements{Fire = 4, Lightning = 1, Water = 2, Earth = 3}
	public int[] playerChoose;
	public int[] botChoose;
	public Sprite[] ElementImage;
	public GameObject[] Shadow;
	public GameObject[] playerChooseimage;
	public GameObject[] botChooseimage;
	public PlayerHpSliderScript playerHP;
	public EnemyHpSliderScript EnemyHP;
	public int timming;
	public int currentTurn;
	//private bool playersTurn = true;
	public Text[] tex;
	public Text[] para;

	void Start(){
		currentTurn = GameManagerScript.gameManager.turn;
		StartCoroutine (PlayerChoose(timming));
		StartCoroutine (BotChoose(timming));
		StartCoroutine (CheckWinner(timming));

	}

//	void Restart(){
//		PlayerChoose ();
//		BotChoose ();
//		Once = true;
//		CheckWinner ();
//		Turn = 0;
//	}
//
//	void Update(){
//		if (Input.GetMouseButtonDown (1)) {
//			Restart ();
//		}
//
//		if (Once) {
//			
//		}
//
//		if (Input.GetKeyDown (KeyCode.Space)) {
//			PlayerChoose ();
//			BotChoose ();
//			Once = true;
//			for(int TheTurn = Turn; Turn <= 3; Turn++){
//				StartCoroutine (IsTurn (2f, TheTurn));
//			}
//			Turn = 0;
//		}
//	}
		

	#region Enum

	IEnumerator IsTurn(float time, int turn){



		yield return new WaitForSeconds (time);
	}

	#endregion

	public void EnemyExample(){
		
	}
		

		
	IEnumerator CheckWinner(int waiting){
		
	playerHP = playerHP.GetComponent<PlayerHpSliderScript> ();
	EnemyHP = EnemyHP.GetComponent<EnemyHpSliderScript> ();

		yield return new WaitForSeconds (waiting);
		for (int Slot=0; Slot<=2; Slot++) { 
		
			if (playerChoose[Slot] == botChoose [Slot]) {
				tex[Slot].text = "Draw";
				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
			}

			else if(playerChoose[Slot] == 4 && botChoose[Slot] == 1){
				tex[Slot].text = "Effect No";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					playerHP.Damage ();
					EnemyHP.Damage ();
				}
			}
			else if(playerChoose[Slot] == 4 && botChoose[Slot] == 2){
				tex[Slot].text = "DoubleDamage Water";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					playerHP.Damage ();
					playerHP.Damage ();
					EnemyHP.Damage ();
				}
			}
			else if(playerChoose[Slot] == 4 && botChoose[Slot] == 3){
				tex[Slot].text = "Burned Earth";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					GameManagerScript.gameManager.burnTurn [Slot+3] = GameManagerScript.gameManager.turn + 2;
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					GameManagerScript.gameManager.burnTurn [Slot + 3] = GameManagerScript.gameManager.turn + 2;
					playerHP.Damage ();
					EnemyHP.Damage ();
				}
			}

			else if(playerChoose[Slot] == 1 && botChoose[Slot] == 4){
				tex[Slot].text = "Effect No";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					playerHP.Damage ();
					EnemyHP.Damage ();
				}
			}
			else if(playerChoose[Slot] == 1 && botChoose[Slot] == 2){
				tex[Slot].text = "Paralyzed Water";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
				GameManagerScript.gameManager.paralyzedTurn[Slot+3] = GameManagerScript.gameManager.turn + 1;

				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					playerHP.Damage ();
					EnemyHP.Damage ();
				}
			}
			else if(playerChoose[Slot] == 1 && botChoose[Slot] == 3){
				tex[Slot].text = "Miss Lightning";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);

				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					playerHP.Damage ();
				}
			}

			else if(playerChoose[Slot] == 2 && botChoose[Slot] == 4){
				tex[Slot].text = "DoubleDamage Water";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					playerHP.Damage ();
					EnemyHP.Damage ();
					EnemyHP.Damage ();
				}
			}
			else if(playerChoose[Slot] == 2 && botChoose[Slot] == 1){
				tex[Slot].text = "Paralyzed Water";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
				GameManagerScript.gameManager.paralyzedTurn[Slot] = GameManagerScript.gameManager.turn + 1;

				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					playerHP.Damage ();
					EnemyHP.Damage ();
				}
			}
			else if(playerChoose[Slot] == 2 && botChoose[Slot] == 3){
				
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					tex[Slot].text = "Healed";
					EnemyHP.Heal();
					playerHP.Damage ();
					EnemyHP.Damage ();
				}
			}
			else if(playerChoose[Slot] == 3 && botChoose[Slot] == 4){
				tex[Slot].text = "Burned Earth";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);
				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					GameManagerScript.gameManager.burnTurn [Slot] = GameManagerScript.gameManager.turn + 2;
					playerHP.Damage ();
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					GameManagerScript.gameManager.burnTurn [Slot] = GameManagerScript.gameManager.turn + 2;
					playerHP.Damage ();
					EnemyHP.Damage ();
				}
			}
			else if(playerChoose[Slot] == 3 && botChoose[Slot] == 1){
				tex[Slot].text = "Miss Lightning";
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);

				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					EnemyHP.Damage ();
				}
			}
			else if(playerChoose[Slot] == 3 && botChoose[Slot] == 2){
				
				playerHP.Burned (Slot);
				EnemyHP.Burned (Slot + 3);

				if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot]){
					playerHP.Damage ();
					para[Slot].text = "Paralyzed";
					Debug.Log ("Player Stuned");
				}
				else if(GameManagerScript.gameManager.turn == GameManagerScript.gameManager.paralyzedTurn[Slot+3]){
					EnemyHP.Damage ();
					para[Slot+3].text = "Paralyzed";
					Debug.Log ("Enemy Stuned");
				}
				else{
					tex[Slot].text = "Healed";
					playerHP.Heal();
					playerHP.Damage ();
					EnemyHP.Damage ();
				}	
			}

			yield return new WaitForSeconds (waiting);
		}

//		if (playerChoose[1] == botChoose [1]) {
//			tex[1].text = "Draw";
//			if (Once) {
//				playerHP.Damage ();
//				EnemyHP.Damage ();
//				Once = false;
//			}
//			Once = true;
//		}
//		if (playerChoose[2] == botChoose [2]) {
//			tex[2].text = "Draw";
//			if (Once) {
//				playerHP.Damage ();
//				EnemyHP.Damage ();
//				Once = false;
//			}
//			Once = true;
//		}



	}

	IEnumerator PlayerChoose(int waiting){
		yield return new WaitForSeconds (waiting);
		for(int i = 0 ; i<=2 ; i++){
			playerChoose [i] = GameManagerScript.gameManager.slot [i];
				Debug.Log ("PlayerA = "+playerChoose[i]);
			playerChooseimage[i].GetComponent<Image> ().sprite = ElementImage [playerChoose[i]];

			yield return new WaitForSeconds (waiting);
		}

//		do{
//			playerChoose[1] = Random.Range(0,4);
//			Debug.Log ("PlayerB = "+playerChoose[1]);
//		}while(playerChoose[1] == playerChoose[0]);
//		playerChooseimage[1].GetComponent<Image> ().sprite = ElementImage [playerChoose[1]];
//
//		do{
//			playerChoose[2] = Random.Range(0,4);
//			Debug.Log ("PlayerC = "+playerChoose[2]);
//		}while(playerChoose[2] == playerChoose[0] || playerChoose[2] == playerChoose[1]);
//		playerChooseimage[2].GetComponent<Image> ().sprite = ElementImage [playerChoose[2]];



	}
	IEnumerator BotChoose(int waiting){
		yield return new WaitForSeconds (waiting);

		botChoose[0] = Random.Range(1,5);
		Debug.Log ("BotA = "+botChoose[0]);
		botChooseimage[0].GetComponent<Image> ().sprite = ElementImage [botChoose[0]];
		Shadow [0].SetActive (true);
		Shadow [1].SetActive (true);
		yield return new WaitForSeconds (waiting);

		do{
			botChoose[1] = Random.Range(1,5);
			Debug.Log ("BotB = "+botChoose[1]);
		}while(botChoose[1] == botChoose[0]);
		botChooseimage[1].GetComponent<Image> ().sprite = ElementImage [botChoose[1]];
		Shadow [2].SetActive (true);
		Shadow [3].SetActive (true);
		yield return new WaitForSeconds (waiting);

		do{
			botChoose[2] = Random.Range(1,5);
			Debug.Log ("BotC = "+botChoose[2]);
		}while(botChoose[2] == botChoose[0] || botChoose[2] == botChoose[1]);
		botChooseimage[2].GetComponent<Image> ().sprite = ElementImage [botChoose[2]];
		Shadow [4].SetActive (true);
		Shadow [5].SetActive (true);
	}


}
