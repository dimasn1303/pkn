﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene2Script : MonoBehaviour {

	public void GotoScene2(){
		SceneManager.LoadScene ("Scene2");
	}

	public void GotoScene1(){
		SceneManager.LoadScene("Scene1");
	}
}