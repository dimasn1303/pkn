﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene1Script : MonoBehaviour {

	void OnGUI(){
		GUI.Label (new Rect (Screen.width/2, Screen.height-80, 150, 30), "Scene 1");
		if(GUI.Button (new Rect (Screen.width / 2, Screen.height-50, 100, 30), "Go to Scene 2")){
			SceneManager.LoadScene ("Scene2");
		}
	}
}