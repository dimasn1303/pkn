﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour {

	public static GameManagerScript gameManager;

	void Awake(){
		if (gameManager == null) {
			DontDestroyOnLoad (gameObject);
			gameManager = this;
		}
		else if(gameManager !=this){
			Destroy (this);
		}
	}

}
